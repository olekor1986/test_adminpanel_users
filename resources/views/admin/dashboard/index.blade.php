@extends('templates.admin')

@section('header')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class="h3">Users count by Age</h3>
            <div id="chartdiv_0"></div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class="h3">Growth rate ry Users Count</h3>
            <div id="chartdiv_1"></div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h4>Total count of Users - <span class="badge badge-secondary">{{$usersCount}}</span></h4>
        </div>
    </div>
    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <!-- Users By Age Chart -->
    <script>
        am4core.ready(function() {
// Themes begin
            am4core.useTheme(am4themes_animated);
// Create chart instance
            var chart = am4core.create("chartdiv_0", am4charts.XYChart);
// Add data
            chart.data = <?=$usersByAgeJson?>;
// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "age";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
// Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "count";
            series.dataFields.categoryX = "age";
            series.name = "Count";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;
            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
        });
    </script>
    <!-- End Users By Age Chart -->
    <!-- Users By Create Date Chart -->
    <script>
        am4core.ready(function() {
// Themes begin
            am4core.useTheme(am4themes_animated);
// Create chart instance
            var chart = am4core.create("chartdiv_1", am4charts.XYChart);
// Add data
            chart.data = <?=$growthRateByUsersJson?>;
// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "created_at";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
// Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "count";
            series.dataFields.categoryX = "created_at";
            series.name = "Count";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;
            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
        });
    </script>
    <!-- End Users By Create Date Chart -->
@endsection
