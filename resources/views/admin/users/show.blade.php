@extends('templates.admin')

@section('header')
<h2 class="h2">User data {{$user->name}}</h2>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Id
                <span class="badge badge-primary badge-pill">{{$user->id}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Login
                <span class="badge badge-primary badge-pill">{{$user->name}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Email
                <span class="badge badge-primary badge-pill">{{$user->email}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Created At
                <span class="badge badge-primary badge-pill">{{$user->created_at}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                First Name
                <span class="badge badge-primary badge-pill">{{$user->first_name}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Last Name
                <span class="badge badge-primary badge-pill">{{$user->last_name}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Age
                <span class="badge badge-primary badge-pill">{{$user->age}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Phone
                <span class="badge badge-primary badge-pill">{{$user->phone}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Country
                <span class="badge badge-primary badge-pill">{{$user->country}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                City
                <span class="badge badge-primary badge-pill">{{$user->city}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Level
                <span class="badge badge-primary badge-pill">{{$user->level}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Banned
                <span class="badge badge-primary badge-pill">{{$user->banned}}</span>
            </li>
        </ul>
    </div>
</div>
@endsection
