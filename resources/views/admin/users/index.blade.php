@extends('templates.admin')

@section('header')
        <h2 class="h2">Users</h2>
        <a href="/users/create" role="button" class="btn btn-lg btn-primary">Create User</a>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="table-responsive-sm">
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Login</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created At</th>
                {{--
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Country</th>
                    <th scope="col">City</th>
                    <th scope="col">Level</th>
                    <th scope="col">Banned</th>
                --}}
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                    {{--
                        <td>{{$user->first_name}}</td>
                        <td>{{$user->last_name}}</td>
                        <td>{{$user->age}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->country}}</td>
                        <td>{{$user->city}}</td>
                        <td>{{$user->level}}</td>
                        <td>{{$user->banned}}</td>
                    --}}
                        <td>
                            <a href="/users/{{$user->id}}" class="btn btn-sm btn-success" role="button">Show</a>
                        </td>
                        <td>
                            <a href="/users/{{$user->id}}/edit" class="btn btn-sm btn-warning" role="button">Edit</a>
                        </td>
                        <td>
                            <form action="/users/{{$user->id}}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-sm btn-danger">Del</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
