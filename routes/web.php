<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/***Home Route***/
Route::get('/', function (){
    return redirect('/users');
});

/***Users Route***/
Route::resource('users', 'Admin\UsersController');

/***Dashboard Route***/
Route::resource('dashboard', 'Admin\DashboardController');
