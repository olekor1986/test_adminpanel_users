<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            "name" => "evgeniya.prohorova",
            "email" => "evgeniya.prohorova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-06 20:52:28",
            "first_name" => "Evgeniya",
            "last_name" => "Prohorova",
            "age" => "31",
            "phone" => "654-65-80",
            "country" => "Russian Federation",
            "city" => "Rostov-Na-Donu",
            "level" => 2,
            "banned" => 0
        ],
        [
            "name" => "maksim.zolotov",
            "email" => "maksim.zolotov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-06 20:52:28",
            "first_name" => "Maksim",
            "last_name" => "Zolotov",
            "age" => "31",
            "phone" => "305-16-14",
            "country" => "Ukraine",
            "city" => "Dnepr",
            "level" => 3,
            "banned" => 0
        ],
        [
            "name" => "filipp.matveev",
            "email" => "filipp.matveev@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-06 20:52:28",
            "first_name" => "Filipp",
            "last_name" => "Matveev",
            "age" => "26",
            "phone" => "895-28-92",
            "country" => "Ukraine",
            "city" => "Kharkiv",
            "level" => 5,
            "banned" => 0
        ],
        [
            "name" => "dmitriy.romanov",
            "email" => "dmitriy.romanov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-06 20:52:28",
            "first_name" => "Dmitriy",
            "last_name" => "Romanov",
            "age" => "18",
            "phone" => "856-76-20",
            "country" => "Ukraine",
            "city" => "Dnepr",
            "level" => 7,
            "banned" => 0
        ],
        [
            "name" => "artem.zavyalov",
            "email" => "artem.zavyalov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Artem",
            "last_name" => "Zavyalov",
            "age" => "27",
            "phone" => "655-59-62",
            "country" => "USA",
            "city" => "Detroit",
            "level" => 2,
            "banned" => 0
        ],
        [
            "name" => "aleksandr.kalinin",
            "email" => "aleksandr.kalinin@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Aleksandr",
            "last_name" => "Kalinin",
            "age" => "32",
            "phone" => "512-81-23",
            "country" => "Belarus",
            "city" => "Mogilev",
            "level" => 9,
            "banned" => 0
        ],
        [
            "name" => "miya.zolotova",
            "email" => "miya.zolotova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Miya",
            "last_name" => "Zolotova",
            "age" => "32",
            "phone" => "666-40-75",
            "country" => "Belarus",
            "city" => "Minsk",
            "level" => 1,
            "banned" => 0
        ],
        [
            "name" => "aleksandra.ustinova",
            "email" => "aleksandra.ustinova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Aleksandra",
            "last_name" => "Ustinova",
            "age" => "30",
            "phone" => "295-34-78",
            "country" => "Ukraine",
            "city" => "Kharkiv",
            "level" => 6,
            "banned" => 0
        ],
        [
            "name" => "mariya.egorova",
            "email" => "mariya.egorova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Mariya",
            "last_name" => "Egorova",
            "age" => "30",
            "phone" => "854-56-36",
            "country" => "Belarus",
            "city" => "Minsk",
            "level" => 7,
            "banned" => 0
        ],
        [
            "name" => "konstantin.orlov",
            "email" => "konstantin.orlov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Konstantin",
            "last_name" => "Orlov",
            "age" => "30",
            "phone" => "847-81-73",
            "country" => "Russian Federation",
            "city" => "Rostov-Na-Donu",
            "level" => 5,
            "banned" => 0
        ],
        [
            "name" => "artur.tarasov",
            "email" => "artur.tarasov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Artur",
            "last_name" => "Tarasov",
            "age" => "26",
            "phone" => "262-64-82",
            "country" => "Belarus",
            "city" => "Brest",
            "level" => 5,
            "banned" => 0
        ],
        [
            "name" => "ilya.demyanov",
            "email" => "ilya.demyanov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-07 20:52:28",
            "first_name" => "Ilya",
            "last_name" => "Demyanov",
            "age" => "26",
            "phone" => "117-25-24",
            "country" => "Russian Federation",
            "city" => "St.Peterburg",
            "level" => 2,
            "banned" => 0
        ],
        [
            "name" => "daniil.chizhov",
            "email" => "daniil.chizhov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-08 20:52:28",
            "first_name" => "Daniil",
            "last_name" => "Chizhov",
            "age" => "32",
            "phone" => "942-68-70",
            "country" => "Ukraine",
            "city" => "Kharkiv",
            "level" => 5,
            "banned" => 0
        ],
        [
            "name" => "polina.muhina",
            "email" => "polina.muhina@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-09 20:52:28",
            "first_name" => "Polina",
            "last_name" => "Muhina",
            "age" => "26",
            "phone" => "578-78-31",
            "country" => "Russian Federation",
            "city" => "Rostov-Na-Donu",
            "level" => 8,
            "banned" => 0
        ],
        [
            "name" => "anna.gorohova",
            "email" => "anna.gorohova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-09 20:52:28",
            "first_name" => "Anna",
            "last_name" => "Gorohova",
            "age" => "26",
            "phone" => "100-80-17",
            "country" => "USA",
            "city" => "Los-Angeles",
            "level" => 6,
            "banned" => 0
        ],
        [
            "name" => "alisa.ustinova",
            "email" => "alisa.ustinova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-09 20:52:28",
            "first_name" => "Alisa",
            "last_name" => "Ustinova",
            "age" => "65",
            "phone" => "855-62-16",
            "country" => "Russian Federation",
            "city" => "Volgograd",
            "level" => 10,
            "banned" => 0
        ],
        [
            "name" => "artem.astafev",
            "email" => "artem.astafev@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-09 20:52:28",
            "first_name" => "Artem",
            "last_name" => "Astafev",
            "age" => "24",
            "phone" => "718-35-83",
            "country" => "Russian Federation",
            "city" => "Rostov-Na-Donu",
            "level" => 3,
            "banned" => 0
        ],
        [
            "name" => "vladislav.sergeev",
            "email" => "vladislav.sergeev@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-09 20:52:28",
            "first_name" => "Vladislav",
            "last_name" => "Sergeev",
            "age" => "26",
            "phone" => "831-93-61",
            "country" => "USA",
            "city" => "Chicago",
            "level" => 2,
            "banned" => 0
        ],
        [
            "name" => "viktoriya.ilina",
            "email" => "viktoriya.ilina@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-10 20:52:28",
            "first_name" => "Viktoriya",
            "last_name" => "Ilina",
            "age" => "27",
            "phone" => "778-88-88",
            "country" => "Ukraine",
            "city" => "Lviv",
            "level" => 7,
            "banned" => 0
        ],
        [
            "name" => "artemiy.kalinin",
            "email" => "artemiy.kalinin@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-10 20:52:28",
            "first_name" => "Artemiy",
            "last_name" => "Kalinin",
            "age" => "26",
            "phone" => "524-22-20",
            "country" => "Ukraine",
            "city" => "Lviv",
            "level" => 3,
            "banned" => 0
        ],
        [
            "name" => "eva.andreeva",
            "email" => "eva.andreeva@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Eva",
            "last_name" => "Andreeva",
            "age" => "19",
            "phone" => "353-65-91",
            "country" => "Russian Federation",
            "city" => "Moskow",
            "level" => 1,
            "banned" => 0
        ],
        [
            "name" => "zahar.vasilev",
            "email" => "zahar.vasilev@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Zahar",
            "last_name" => "Vasilev",
            "age" => "32",
            "phone" => "978-17-52",
            "country" => "Ukraine",
            "city" => "Vynnitsa",
            "level" => 4,
            "banned" => 0
        ],
        [
            "name" => "ruslan.sitnikov",
            "email" => "ruslan.sitnikov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Ruslan",
            "last_name" => "Sitnikov",
            "age" => "22",
            "phone" => "290-27-63",
            "country" => "USA",
            "city" => "Los-Angeles",
            "level" => 2,
            "banned" => 0
        ],
        [
            "name" => "mark.vasilev",
            "email" => "mark.vasilev@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Mark",
            "last_name" => "Vasilev",
            "age" => "36",
            "phone" => "701-66-17",
            "country" => "USA",
            "city" => "Washington",
            "level" => 8,
            "banned" => 0
        ],
        [
            "name" => "timofey.sofronov",
            "email" => "timofey.sofronov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Timofey",
            "last_name" => "Sofronov",
            "age" => "22",
            "phone" => "472-77-99",
            "country" => "Belarus",
            "city" => "Mogilev",
            "level" => 4,
            "banned" => 0
        ],
        [
            "name" => "fedor.volkov",
            "email" => "fedor.volkov@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Fedor",
            "last_name" => "Volkov",
            "age" => "18",
            "phone" => "284-37-39",
            "country" => "USA",
            "city" => "New-York",
            "level" => 9,
            "banned" => 0
        ],
        [
            "name" => "mariya.soboleva",
            "email" => "mariya.soboleva@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Mariya",
            "last_name" => "Soboleva",
            "age" => "18",
            "phone" => "671-87-40",
            "country" => "Ukraine",
            "city" => "Vynnitsa",
            "level" => 3,
            "banned" => 0
        ],
        [
            "name" => "maksim.kalinin",
            "email" => "maksim.kalinin@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Maksim",
            "last_name" => "Kalinin",
            "age" => "18",
            "phone" => "136-78-73",
            "country" => "Ukraine",
            "city" => "Kyiv",
            "level" => 10,
            "banned" => 0
        ],
        [
            "name" => "aleksey.demin",
            "email" => "aleksey.demin@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Aleksey",
            "last_name" => "Demin",
            "age" => "22",
            "phone" => "134-92-87",
            "country" => "USA",
            "city" => "Detroit",
            "level" => 10,
            "banned" => 0
        ],
        [
            "name" => "sofya.evdokimova",
            "email" => "sofya.evdokimova@mail.com",
            "password" => "12345678",
            "created_at" => "2020-07-11 20:52:28",
            "first_name" => "Sofya",
            "last_name" => "Evdokimova",
            "age" => "47",
            "phone" => "789-29-71",
            "country" => "Russian Federation",
            "city" => "Vladivostok",
            "level" => 5,
            "banned" => 0
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [];
        foreach ($this->users as $key => $value) {
            $array[$key]['name'] = $value['name'];
            $array[$key]['email'] = $value['email'];
            $array[$key]['password'] = Hash::make($value['password']);
            $array[$key]['created_at'] = $value['created_at'];
            $array[$key]['first_name'] = $value['first_name'];
            $array[$key]['last_name'] = $value['last_name'];
            $array[$key]['age'] = $value['age'];
            $array[$key]['phone'] = $value['phone'];
            $array[$key]['country'] = $value['country'];
            $array[$key]['city'] = $value['city'];
            $array[$key]['level'] = $value['level'];
            $array[$key]['banned'] = boolval($value['banned']);

        }
        DB::table('users')->insert($array);
    }
}
