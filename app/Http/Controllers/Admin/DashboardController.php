<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $usersCount = (new \App\User)->getUsersCount();
        $usersByAge =  (new \App\User)->getUsersCount('age');
        $growthRateByUsers = (new \App\User)->getGrowthRateByUsersCount();
        $usersByAgeJson = json_encode($usersByAge);
        $growthRateByUsersJson = json_encode($growthRateByUsers);
        return view('admin.dashboard.index',
            compact('usersByAgeJson', 'growthRateByUsersJson', 'usersCount')
        );
    }
}
