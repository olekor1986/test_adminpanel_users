<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:255',
            'email' => 'required|email',
            'password' => 'min:8',
            'first_name' => 'min:4|max:255',
            'last_name' => 'min:4|max:255',
            'age' => 'min:2|max:2',
            'phone' => 'min:4|max:16',
            'country' => 'min:2|max:255',
            'city' => 'min:2|max:255',
            'level' => 'min:1|max:2',
            'banned' => 'min:1|max:1'
        ];
    }
}
