<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'age', 'phone', 'country',
        'city', 'level', 'banned'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUsersCount($property = 'count')
    {
        if($property == 'count') {
            return count(User::all());
        }
        try{
            $usersCount = [];
            $users = User::all()->sortBy($property);
            $properties = $users->unique($property)->pluck($property);
            foreach ($properties as $item){
                $usersFromAge = $users->where($property, 'like', $item);
                $array[$property] = $item;
                $array['count'] = count($usersFromAge);
                $usersCount[] = $array;
            }
            return $usersCount;
        } catch (\Exception $e){
            echo 'Error: ',  $e->getMessage();
        }
    }

    public function getGrowthRateByUsersCount()
    {
        $usersByDate =  (new \App\User)->getUsersCount('created_at');
        $growthRateByUsers = [];
        $sum = 0;
        foreach($usersByDate as $item){
            $sum += $item['count'];
            $date = Carbon::parse($item['created_at']);
            $array['created_at'] = $date->isoFormat('YYYY-DD-MM');
            $array['count'] = $sum;
            $growthRateByUsers[] = $array;
        }
        return $growthRateByUsers;
    }
}
