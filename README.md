Server configuration:
PHP 7.3
Apache 2.4
MySQL 5.7

Before starting work:

1. execute command: git clone https://olekor1986@bitbucket.org/olekor1986/test_adminpanel_users.git
2. execute command: composer install
3. when setting up a virtual host, specify the path to the working directory /public
4. create a MySQL database
5. copy the .env.example file, then rename it to .env and specify the database access settings
6. execute command: php artisan key:generate
7. execute command: php artisan migrate
8. execute command: php artisan db:seed

all is ready!

